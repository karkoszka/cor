package net.karkoszka.domain;

public class Car {

    private boolean isChangedByVarnisher = false;
    private boolean isChangedByElectrician = false;
    private boolean isChangedByTinsmith = false;
    private boolean isChangedByMechanic = false;

    public boolean isChangedByMechanic() {
        return isChangedByMechanic;
    }

    public void setChangedByMechanic(boolean changedByMechanic) {
        isChangedByMechanic = changedByMechanic;
    }

    public boolean isChangedByTinsmith() {
        return isChangedByTinsmith;
    }

    public void setChangedByTinsmith(boolean changedByTinsmith) {
        isChangedByTinsmith = changedByTinsmith;
    }

    public boolean isChangedByElectrician() {
        return isChangedByElectrician;
    }

    public void setChangedByElectrician(boolean changedByElectrician) {
        isChangedByElectrician = changedByElectrician;
    }

    public boolean isChangedByVarnisher() {
        return isChangedByVarnisher;
    }

    public void setChangedByVarnisher(boolean changedByVarnisher) {
        isChangedByVarnisher = changedByVarnisher;
    }
}
