package net.karkoszka.domain;

public class Order {

    private boolean isVarnisherWork = false;
    private boolean isElectricianWork = false;
    private boolean isTinsmithWork = false;
    private boolean isMechanicWork = false;
    private Car car;

    public Order(Car car, boolean varnisherWork, boolean electricianWork, boolean tinsmithWork, boolean mechanicWork) {
        isVarnisherWork = varnisherWork;
        isElectricianWork = electricianWork;
        isTinsmithWork = tinsmithWork;
        isMechanicWork = mechanicWork;
        this.car = car;
    }

    public Order(Car car) {
        this.car = car;
    }

    public boolean isVarnisherWork() {
        return isVarnisherWork;
    }

    public boolean isMechanicWork() {
        return isMechanicWork;
    }

    public boolean isTinsmithWork() {
        return isTinsmithWork;
    }

    public boolean isElectricianWork() {
        return isElectricianWork;
    }

    public void setElectricianWork(boolean electricianWork) {
        isElectricianWork = electricianWork;
    }

    public void setMechanicWork(boolean mechanicWork) {
        isMechanicWork = mechanicWork;
    }

    public void setTinsmithWork(boolean tinsmithWork) {
        isTinsmithWork = tinsmithWork;
    }

    public void setVarnisherWork(boolean varnisherWork) {
        isVarnisherWork = varnisherWork;
    }

    public Car getCar() {
        return car;
    }
}
