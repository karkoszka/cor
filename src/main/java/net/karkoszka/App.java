package net.karkoszka;

import net.karkoszka.domain.Car;
import net.karkoszka.domain.Order;
import net.karkoszka.runner.WorkRunner;

public class App {
    public static void main(String[] args) {
        Car car = new Car();

        Order order = new Order(car);
        order.setTinsmithWork(true);
        order.setElectricianWork(true);

        WorkRunner workRunner = new WorkRunner();
        workRunner.execute(order);
    }
}
