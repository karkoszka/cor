package net.karkoszka.work;

import net.karkoszka.domain.Order;

public class TinsmithWork extends Work {

    public TinsmithWork(Work work) {
        super(work);
    }

    public Order execute(Order order) {
        if (null != order && order.isTinsmithWork()) {
            order.getCar().setChangedByTinsmith(true);
            System.out.println("Tinsmith did something");
        }
        return super.execute(order);
    }

}
