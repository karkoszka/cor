package net.karkoszka.work;

import net.karkoszka.domain.Order;

public class VarnisherWork extends Work {

    public VarnisherWork(Work work) {
        super(work);
    }

    public Order execute(Order order) {
        if (null != order && order.isVarnisherWork()) {
            order.getCar().setChangedByVarnisher(true);
            System.out.println("Varnisher did something");
        }
        return super.execute(order);
    }

}
