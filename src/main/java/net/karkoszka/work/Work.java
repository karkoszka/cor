package net.karkoszka.work;

import net.karkoszka.domain.Order;

public class Work {

    protected Work nextWork;

    public Work() {
        this.nextWork = null;
    }

    public Work(Work nextWork) {
        this.nextWork = nextWork;
    }

    public Order execute(Order order) {
        if (null == nextWork) {
            System.out.println("Nothing more to do");
        } else {
            nextWork.execute(order);
        }
        return order;
    }
}
