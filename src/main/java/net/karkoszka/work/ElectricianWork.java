package net.karkoszka.work;

import net.karkoszka.domain.Order;

public class ElectricianWork extends Work {

    public ElectricianWork(Work work) {
        super(work);
    }

    public Order execute(Order order) {
        if (null != order && order.isElectricianWork()) {
            order.getCar().setChangedByElectrician(true);
            System.out.println("Electrician did something");
        }
        return super.execute(order);
    }

}
