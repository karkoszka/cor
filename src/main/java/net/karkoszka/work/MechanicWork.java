package net.karkoszka.work;

import net.karkoszka.domain.Order;

public class MechanicWork extends Work {

    public MechanicWork(Work work) {
        super(work);
    }

    public Order execute(Order order) {
        if (null != order && order.isMechanicWork()) {
            order.getCar().setChangedByMechanic(true);
            System.out.println("Mechanic did something");
        }
        return super.execute(order);
    }

}
