package net.karkoszka.runner;

import net.karkoszka.domain.Order;
import net.karkoszka.work.*;

public class WorkRunner {

    public Order execute(Order order) {
        Work work = new Work();
        VarnisherWork varnisherWork = new VarnisherWork(work);
        TinsmithWork tinsmithWork = new TinsmithWork(varnisherWork);
        MechanicWork mechanicWork = new MechanicWork(tinsmithWork);
        ElectricianWork electricianWork = new ElectricianWork(mechanicWork);
        return electricianWork.execute(order);
    }
}
