package net.karkoszka;

import junit.framework.Assert;
import net.karkoszka.domain.Car;
import net.karkoszka.domain.Order;
import net.karkoszka.runner.WorkRunner;
import org.junit.Test;

import static org.easymock.EasyMock.createNiceMock;

public class WorkRunnerTest {

    @Test
    public void shouldOnlyElectricianDidWork() {
        //Given
        WorkRunner workRunner = new WorkRunner();
        Car car = new Car();
        Order order = new Order(car);
        order.setElectricianWork(true);

        //When
        workRunner.execute(order);

        //Then
        Assert.assertTrue(order.getCar().isChangedByElectrician());
        Assert.assertFalse(order.getCar().isChangedByMechanic());
        Assert.assertFalse(order.getCar().isChangedByTinsmith());
        Assert.assertFalse(order.getCar().isChangedByVarnisher());
    }

    @Test
    public void shouldOnlyMechanicDidWork() {
        //Given
        WorkRunner workRunner = new WorkRunner();
        Car car = new Car();
        Order order = new Order(car);
        order.setMechanicWork(true);

        //When
        workRunner.execute(order);

        //Then
        Assert.assertFalse(order.getCar().isChangedByElectrician());
        Assert.assertTrue(order.getCar().isChangedByMechanic());
        Assert.assertFalse(order.getCar().isChangedByTinsmith());
        Assert.assertFalse(order.getCar().isChangedByVarnisher());
    }

    @Test
    public void shouldOnlyTinsmithDidWork() {
        //Given
        WorkRunner workRunner = new WorkRunner();
        Car car = new Car();
        Order order = new Order(car);
        order.setTinsmithWork(true);

        //When
        workRunner.execute(order);

        //Then
        Assert.assertFalse(order.getCar().isChangedByElectrician());
        Assert.assertFalse(order.getCar().isChangedByMechanic());
        Assert.assertTrue(order.getCar().isChangedByTinsmith());
        Assert.assertFalse(order.getCar().isChangedByVarnisher());
    }

    @Test
    public void shouldOnlyVarnisherDidWork() {
        //Given
        WorkRunner workRunner = new WorkRunner();
        Car car = new Car();
        Order order = new Order(car);
        order.setVarnisherWork(true);

        //When
        workRunner.execute(order);

        //Then
        Assert.assertFalse(order.getCar().isChangedByElectrician());
        Assert.assertFalse(order.getCar().isChangedByMechanic());
        Assert.assertFalse(order.getCar().isChangedByTinsmith());
        Assert.assertTrue(order.getCar().isChangedByVarnisher());
    }

    @Test
    public void shouldAllWorkersDidWork() {
        //Given
        WorkRunner workRunner = new WorkRunner();
        Car car = new Car();
        Order order = new Order(car);
        order.setVarnisherWork(true);
        order.setMechanicWork(true);
        order.setTinsmithWork(true);
        order.setElectricianWork(true);

        //When
        workRunner.execute(order);

        //Then
        Assert.assertTrue(order.getCar().isChangedByElectrician());
        Assert.assertTrue(order.getCar().isChangedByMechanic());
        Assert.assertTrue(order.getCar().isChangedByTinsmith());
        Assert.assertTrue(order.getCar().isChangedByVarnisher());
    }

    @Test
    public void shouldNoneWorkersDidWork() {
        //Given
        WorkRunner workRunner = new WorkRunner();
        Car car = new Car();
        Order order = new Order(car);
        order.setVarnisherWork(false);
        order.setMechanicWork(false);
        order.setTinsmithWork(false);
        order.setElectricianWork(false);

        //When
        workRunner.execute(order);

        //Then
        Assert.assertFalse(order.getCar().isChangedByElectrician());
        Assert.assertFalse(order.getCar().isChangedByMechanic());
        Assert.assertFalse(order.getCar().isChangedByTinsmith());
        Assert.assertFalse(order.getCar().isChangedByVarnisher());
    }

    @Test
    public void shouldElectricianAnaMechanicDidWork() {
        //Given
        WorkRunner workRunner = new WorkRunner();
        Car car = new Car();
        Order order = new Order(car);
        order.setVarnisherWork(false);
        order.setMechanicWork(true);
        order.setTinsmithWork(false);
        order.setElectricianWork(true);

        //When
        workRunner.execute(order);

        //Then
        Assert.assertTrue(order.getCar().isChangedByElectrician());
        Assert.assertTrue(order.getCar().isChangedByMechanic());
        Assert.assertFalse(order.getCar().isChangedByTinsmith());
        Assert.assertFalse(order.getCar().isChangedByVarnisher());
    }
}
